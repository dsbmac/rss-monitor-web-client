import * as firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyDzN0n7DAd3jdv8a0JfD5duVWfL6qhy-BA",
  authDomain: "rss-monitor.firebaseapp.com",
  databaseURL: "https://rss-monitor.firebaseio.com",
  projectId: "rss-monitor",
  storageBucket: "rss-monitor.appspot.com",
  messagingSenderId: "981457092013",
  appId: "1:981457092013:web:ab9283ddfec6f07a"
};

const firebaseApp = firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const fireAuth = firebaseApp.auth();
export default firebaseApp;
