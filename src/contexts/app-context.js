import React, { createContext, useState, useEffect, useCallback } from "react";
import * as firebase from "firebase/app";
import FirebaseCtx from "../components/Firebase/context";

const AppCtx = createContext();

// make a functional comp return the provider
function AppProvider(props) {
  const [state, setState] = useState(null);

  const handleSettings = useCallback(
    snap => {
      const newSettings = snap.val();
      setState({ userSettings: newSettings, userId: props.user.uid });
    },
    [props.user.uid]
  );

  useEffect(() => {
    const settingsRef = firebase.database().ref(`users/${props.user.uid}`);
    settingsRef.on("value", handleSettings);
    return () => {
      settingsRef.off("value", handleSettings);
    };
  }, [handleSettings, props.user.uid]);

  return <AppCtx.Provider value={state}>{props.children}</AppCtx.Provider>;
}

export { AppProvider };
export default AppCtx;
