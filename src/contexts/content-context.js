import React, {
  createContext,
  useContext,
  useState,
  useEffect,
  useCallback
} from "react";
import * as firebase from "firebase/app";
import { clone } from "lodash";
import AppCtx from "./app-context";
import { calcDistance } from "../utils";

const ContentCtx = createContext();

// make a functional comp return the provider
function ContentProvider(props) {
  const userCtx = useContext(AppCtx);
  const [userSettings, setUserSettings] = useState(false);
  const [ads, setAds] = useState([]);
  const [adKeys, setAdKeys] = useState([]);
  const [filteredAds, setFilteredAds] = useState([]);
  const [state, setState] = useState({ ads: [], userSettings: {} });
  // const feedKey = "-Lg8Wg1v1rl9-i-i_O5U";
  const [feedKeys, setFeedKeys] = useState([]);
  const [subscribedFeeds, setSubscribedFeeds] = useState(false);
  const [distanceLimit, setDistanceLimit] = useState(null);
  const [isDistanceEnabled, setIsDistanceEnabled] = useState(null);
  const [geoLat, setGeoLat] = useState(null);
  const [geoLong, setGeoLong] = useState(null);

  useEffect(() => {
    if (subscribedFeeds) {
      const newFeedKeys = Object.keys(subscribedFeeds);
      if (JSON.stringify(newFeedKeys) !== JSON.stringify(feedKeys)) {
        setFeedKeys(newFeedKeys);
      }
    }
  }, [feedKeys, subscribedFeeds]);

  useEffect(() => {
    if (userCtx) {
      setUserSettings({ ...userCtx.userSettings });
      if (
        JSON.stringify(userCtx.userSettings.subscribedFeeds) !==
        JSON.stringify(subscribedFeeds)
      ) {
        setSubscribedFeeds(userCtx.userSettings.subscribedFeeds);
      }
    }
  }, [distanceLimit, subscribedFeeds, userCtx]);

  useEffect(() => {
    if (
      (userSettings && userSettings.geoLat !== geoLat) ||
      (userSettings && userSettings.geoLong !== geoLong)
    ) {
      setGeoLat(() => userSettings.geoLat);
      setGeoLong(() => userSettings.geoLong);
    }
  }, [geoLat, geoLong, userSettings]);

  const parseDistance = useCallback((entry, userGeoCoords) => {
    const adLat = entry.geo_lat;
    const adLong = entry.geo_long;
    if (adLat && adLong && userGeoCoords) {
      const distance = calcDistance(
        parseFloat(adLat),
        parseFloat(adLong),
        parseFloat(userGeoCoords[0]),
        parseFloat(userGeoCoords[1]),
        "K"
      );
      return Math.ceil(distance);
    }
    return -1;
  }, []);

  const addProperties = useCallback(
    (adData, adKey) => {
      const newAd = clone(adData, true);
      const distance = parseDistance(newAd, [geoLat, geoLong]);
      newAd.distance = distance;
      newAd.adKey = adKey;
      // newAd.feedKey = feedKey;
      return newAd;
    },
    [geoLat, geoLong, parseDistance]
  );

  const handleNewAd = useCallback(
    (snapshot, filters) => {
      if (geoLat && geoLong) {
        const adKey = snapshot.key;
        const adVal = snapshot.val();
        let newAd = adVal;
        newAd = addProperties(adVal, adKey, geoLat, geoLong);
        setAds(prevAds => [newAd, ...prevAds].slice(0, 50));
        // filterCallback([newAd]);
        // setAds(prevAds => [adKey].concat(prevAds));
        // }
      }
    },
    [addProperties, geoLat, geoLong]
  );

  useEffect(() => {
    if (subscribedFeeds) {
      setAds([]);
      setFilteredAds([]);
      Object.values(subscribedFeeds).map(feed => {
        const adsRef = firebase
          .database()
          .ref(`ads/${feed.feedKey}`)
          .limitToLast(10);
        adsRef.off("child_added", handleNewAd);
        adsRef.on("child_added", handleNewAd);
        return () => {
          adsRef.off("child_added", handleNewAd);
        };
      });
    }
  }, [handleNewAd, subscribedFeeds]);

  return (
    <ContentCtx.Provider value={ads}>{props.children}</ContentCtx.Provider>
  );
}

export { ContentProvider };
export default ContentCtx;
