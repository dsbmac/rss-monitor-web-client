import React from "react";
import firebaseApp from "../auth/fireApi";
import { useObject } from "react-firebase-hooks/database";
import { useAuthState } from "react-firebase-hooks/auth";

const { Provider, FirebaseCtxConsumer } = React.createContext();

function FirebaseCtxProvider(props) {
  const [user, initialising, error] = useAuthState(firebaseApp.auth());

  return <Provider value={{}}>{props.children}</Provider>;
}

export { FirebaseCtxProvider, FirebaseCtxConsumer };
