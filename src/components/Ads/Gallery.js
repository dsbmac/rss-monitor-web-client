import * as React from "react";
import Masonry from "react-masonry-component";
import AdCard from "./AdCard";

const masonryOptions = {
  transitionDuration: 0
};

const imagesLoadedOptions = { background: ".my-bg-image-el" };

class Gallery extends React.Component {
  render() {
    const childElements = this.props.elements.map(function(ad) {
      const adId = ad.id.split("/").pop();
      return (
        <div className="adcard-element-class" key={adId}>
          <AdCard {...ad} />
        </div>
      );
    });
    return (
      <Masonry
        className={"my-gallery-class"} // default ''
        options={masonryOptions} // default {}
        disableImagesLoaded={false} // default false
        updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
        imagesLoadedOptions={imagesLoadedOptions} // default {}
      >
        {childElements}
      </Masonry>
    );
  }
}

export default Gallery;
