import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import Card from "@material-ui/core/Card";
import Chip from "@material-ui/core/Chip";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import indigo from "@material-ui/core/colors/indigo";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import Icon from "@material-ui/core/Icon";

const styles = theme => ({
  card: {
    minWidth: 400,
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  actions: {
    display: "flex"
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    margin: -5,
    width: 50,
    height: 50,
    color: "#fff",
    backgroundColor: indigo["900"]
  },
  priceTag: {},
  icon: {
    margin: theme.spacing(2)
  }
});

function createMarkup(htmlString) {
  return { __html: htmlString };
}

class RedFlagAd extends React.Component {
  state = { expanded: false };

  time_ago_string(pub_date_str) {
    const t1 = new Date(pub_date_str);
    const minutes_elapsed = this.diff_minutes(t1).toString();
    var minute_str = minutes_elapsed > 1 ? "minutes" : "minute";
    const time_ago_str = `${minutes_elapsed} ${minute_str} ago`;
    return time_ago_str;
  }

  imageUrl(links) {
    const placeholderUrl = process.env.PUBLIC_URL + "placeholder.jpg";
    var url;
    try {
      url = links[1].href;
    } catch (error) {
      url = placeholderUrl;
    }
    return url;
  }

  decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
  }

  diff_minutes(dt2, dt1 = new Date()) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
  }

  distance_string(ad_lat, ad_long, currentCoords) {
    let str = "";
    if (ad_lat && ad_long && currentCoords) {
      const current_lat = parseFloat(currentCoords[0]);
      const current_long = parseFloat(currentCoords[1]);
      const distance = this.distance(
        parseFloat(ad_lat),
        parseFloat(ad_long),
        current_lat,
        current_long,
        "K"
      );
      str = `<${Math.round(distance)} kms`;
    }
    return str;
  }

  price_string(propsPrice) {
    const price = propsPrice ? propsPrice.split(".")[0] : "N/A";
    return price;
  }

  render() {
    const { classes } = this.props;
    const time_ago_str = this.time_ago_string(this.props.published);
    const price = this.price_string(this.props["g-core_price"]);
    const title = this.decodeHtml(this.props.title);
    const distanceStr = this.distance_string(
      this.props["geo_lat"],
      this.props["geo_long"],
      this.props.currentCoords
    );
    const dangerDiv = (
      <div dangerouslySetInnerHTML={createMarkup(this.props.summary)} />
    );
    return (
      <a
        href={this.props.link}
        style={{ textDecoration: "none" }}
        target="_blank"
        rel="noopener noreferrer"
      >
        <Card className={classes.card} raised={true}>
          <CardHeader title={title} subheader="" />
          <CardContent>{dangerDiv}</CardContent>
        </Card>
      </a>
    );
  }
}

RedFlagAd.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RedFlagAd);
