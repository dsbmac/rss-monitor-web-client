import React from "react";
import Paper from "@material-ui/core/Paper";
import Gallery from "./Gallery";

function AdsPage(props) {
  return (
    <Paper className="background-paper">
      <Gallery elements={props.ads} currentCoords={props.currentCoords} />
    </Paper>
  );
}

export default AdsPage;
