import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import Card from "@material-ui/core/Card";
import Chip from "@material-ui/core/Chip";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import indigo from "@material-ui/core/colors/indigo";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import Icon from "@material-ui/core/Icon";

const styles = theme => ({
  card: {
    minWidth: 370,
    maxWidth: 370
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  actions: {
    display: "flex"
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    margin: -5,
    width: 50,
    height: 50,
    color: "#fff",
    backgroundColor: indigo["900"]
  },
  priceTag: {},
  icon: {
    margin: theme.spacing(1)
  }
});

function PriceTag(props) {
  return (
    <Chip
      icon={<AttachMoneyIcon style={{ fontSize: 14 }} />}
      color="primary"
      label={
        <Typography color="primary" variant="h6" style={{ marginLeft: -4 }}>
          {props.price}
        </Typography>
      }
      variant="outlined"
    />
  );
}

function PriceTagIcon() {
  return (
    <Icon
      className={classNames(styles.icon, "fa fa-plus-circle")}
      color="disabled"
      fontSize="large"
    />
  );
}
class AdCard extends React.Component {
  state = { expanded: false };

  time_ago_string(pub_date_str) {
    const t1 = new Date(pub_date_str);
    const minutes_elapsed = this.diff_minutes(t1).toString();
    var minute_str = minutes_elapsed > 1 ? "min" : "min";
    const time_ago_str = `${minutes_elapsed}${minute_str} ago`;
    return time_ago_str;
  }

  imageUrl(imageUrl) {
    if (imageUrl) return imageUrl;
    else {
      return "./placeholder.jpg";
    }
  }

  decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
  }

  diff_minutes(dt2, dt1 = new Date()) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    let result = Math.abs(Math.round(diff));
    return result;
  }

  distance_string(distance) {
    if (distance === -1) {
      return "n/a";
    } else {
      return `<${Math.round(distance)}km`;
    }
  }

  price_string(propsPrice) {
    if (propsPrice) {
      const price = propsPrice ? propsPrice.split(".")[0] : "N/A";
      return price;
    }
    return propsPrice;
  }
  render() {
    const { classes } = this.props;
    const time_ago_str = this.time_ago_string(this.props.published);
    const price = this.price_string(this.props["g-core_price"]);
    const title = this.decodeHtml(this.props.title);
    const distanceStr = this.distance_string(this.props["distance"]);

    return (
      <Card className={classes.card} raised={true}>
        <CardHeader
          avatar={<PriceTag price={price} className={classes.priceTag} />}
          title={title}
          subheader={`${time_ago_str}, ${distanceStr}`}
        />
        <a href={this.props.link} target="_blank">
          <CardMedia
            className={classes.media}
            image={this.imageUrl(this.props.img_url)}
            title={title}
          />
        </a>
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            {this.decodeHtml(this.props.summary)}
          </Typography>
        </CardContent>
      </Card>
    );
  }
}

AdCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AdCard);
