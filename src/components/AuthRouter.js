import React, { useState, useEffect, useCallback } from "react";
// import firebaseApp, { fireAuth, provider } from "../auth/fireApi";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Welcome from "./Welcome/Welcome";
import MainContent from "./MainContent";
import LoginForm from "../auth/LoginForm";
import SignUp from "../auth/SignUp";
import Dashboard from "./Settings/Dashboard";
import withAuthProtection from "../auth/withAuthProtection";
// import { useSession, useAuth } from "./App/user-context";

// simple wrapper to render small components on the page
const Wrapper = props => (
  <div style={{ maxWidth: 400, padding: 16, margin: "auto" }} {...props} />
);

// protected routes for main app content
const ProtectedProfile = withAuthProtection("/login")(MainContent);
const ProtectedSettings = withAuthProtection("/login")(Dashboard);
function AuthRouter(props) {
  /**
  |--------------------------------------------------
  | Router with Authentication
  | takes cookies from App
  | return component for paths
  |--------------------------------------------------
  */
  const user = useSession();
  const userSettings = {};

  const handleGoogleSignIn = history => () => {
    return fireAuth.signInWithPopup(provider).then(response => {
      return history.push("/profile");
    });
  };

  const handleSignIn = history => (email, password) => {
    return fireAuth.signInWithEmailAndPassword(email, password).then(() => {
      return history.push("/profile");
    });
  };

  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact render={() => <Welcome />} />
        <Route
          path="/login"
          exact
          render={({ history }) => (
            <Wrapper>
              <Link to="/">Home</Link>
              <LoginForm
                onSubmit={handleSignIn(history)}
                onGoogle={handleGoogleSignIn(history)}
              />
            </Wrapper>
          )}
        />
        <Route
          path="/signup"
          exact
          render={({ history }) => (
            <Wrapper>
              <Link to="/">Home</Link>
              <SignUp />
            </Wrapper>
          )}
        />
        <Route
          path="/settings"
          exact
          render={props => (
            <ProtectedSettings
              {...props}
              me={user}
              userSettings={userSettings}
            />
          )}
        />
        <Route
          path="/profile"
          exact
          render={props => (
            <ProtectedProfile
              {...props}
              me={user}
              userSettings={userSettings}
            />
          )}
        />
        <Route
          path="/public"
          exact
          render={() => (
            <Wrapper>
              <Link to="/">Welcome</Link>
              <Typography variant="h4">
                Public Route, anyone can see this page.
              </Typography>
            </Wrapper>
          )}
        />
      </Switch>
    </BrowserRouter>
  );
}

export default AuthRouter;
