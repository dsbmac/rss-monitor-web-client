import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import { TextField, Switch } from "formik-material-ui";
import * as Yup from "yup";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import LocationIcon from "@material-ui/icons/LocationOn";
import InputAdornment from "@material-ui/core/InputAdornment";

const validationSchema = Yup.object().shape({
  username: Yup.string().required()
});

const userSettings = {
  distanceLimit: 50,
  email: "test@gmail.com",
  firstName: "Test",
  geoLat: 45.487718400000006,
  geoLong: -73.5748096,
  isDistanceEnabled: true,
  isPathFilterEnabled: false,
  isSummaryFilterEnabled: false,
  isTitleFitlerEnabled: false,
  lastName: "Guest",
  priceMax: 800,
  priceMin: 0,
  subscribedFeeds: {
    "-Lg8Wg1v1rl9-i-i_O5U": {
      desc: "Bikes from all of Canada Kijiji",
      exclFromPath: ["kids", "enfant", "mountain", "clothes", "soulier"],
      exclFromSummary: ["girafe", "vtt", "junior"],
      exclFromTitle: ["bequille", "rack"],
      feedKey: "-Lg8Wg1v1rl9-i-i_O5U",
      feedUid: "-Lg8Wg1v1rl9-i-i_O5U",
      inclInTitle: [""],
      name: "Bikes",
      url: "oijwoeifjlksjf.com"
    }
  }
};

const initialValues = {
  ...userSettings
};

function FooForm(props) {
  return (
    <Form autoComplete="off">
      <List>
        <ListItem key={"distanceLimit"}>
          <ListItemIcon button="true" onClick={props.toggleDrawer}>
            {<LocationIcon />}
          </ListItemIcon>
          <Field
            component={TextField}
            name="distanceLimit"
            label="Distance"
            type="number"
            //   className={classes.textField}
            InputLabelProps={{
              shrink: true
            }}
            margin="dense"
            InputProps={{
              endAdornment: <InputAdornment position="end">km</InputAdornment>
            }}
          />
          <Field
            component={Switch}
            color="primary"
            name="isDistanceEnabled"
            //   onChange={e => setIsDistanceEnabled(!isDistanceEnabled)}
          />
        </ListItem>
        {/* <FilterWidget toggleDrawer={toggleDrawer} feeds={feeds} /> */}
        {/* {feeds.length > 0 ? (
            <SideForm feedKey={feeds[0][0]} {...feeds[0][1]} />
          ) : (
            false
          )} */}
        <Button>Cancel</Button>
        <Button onClick={props.submitForm} color="primary">
          Save
        </Button>
      </List>
    </Form>
  );
}

function FormWrapper({ component: Component }) {
  return (
    <Formik
      initialValues={initialValues}
      validate={values => {}}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          actions.setSubmitting(false);
        }, 1000);
      }}
      render={({ submitForm, isSubmitting, values, setFieldValue }) => (
        <Component />
      )}
    />
  );
}

export default FormWrapper;
