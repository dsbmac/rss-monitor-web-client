import React, { useContext } from "react";
// import { fireAuth } from "../../auth/fireApi";
var fireAuth = {};
const userContext = React.createContext({
  user: null
});

export const useSession = () => {
  const { user } = useContext(userContext);
  return user;
};

export const useAuth = () => {
  const [state, setState] = React.useState(() => {
    const user = fireAuth.currentUser;
    return { initializing: !user, user };
  });
  function onChange(user) {
    setState({ initializing: false, user });
  }

  React.useEffect(() => {
    // listen for auth state changes
    const unsubscribe = fireAuth.onAuthStateChanged(onChange);
    // unsubscribe to the listener when unmounting
    return () => unsubscribe();
  }, []);

  return state;
};

export default userContext;
