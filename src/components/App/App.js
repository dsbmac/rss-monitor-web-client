import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { CssBaseline, CircularProgress } from "@material-ui/core";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Login from "../Login/Login";
import HomePage from "../HomePage/HomePage";
import Dashboard from "../Dashboard";

// import "./styles.css";
import "./App.css";
import { AppProvider } from "../../contexts/app-context";
import FirebaseCtx from "../Firebase/context";
import { ContentProvider } from "../../contexts/content-context";
import MyForm from "../MyForm";

const theme = createMuiTheme();

const PrivateRoute = ({ component: Component, user, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        user ? (
          <AppProvider user={user}>
            <ContentProvider>
              <Component {...props} />;
            </ContentProvider>
          </AppProvider>
        ) : (
          <Redirect to="/login" />
        )
      }
      // render={props => (
      //   <AppProvider user={user}>
      //     <ContentProvider>
      //       <TestCtxComponent />
      //     </ContentProvider>
      //   </AppProvider>
      // )}
    />
  );
};

export default function App() {
  const firebaseCtx = React.useContext(FirebaseCtx);

  return !firebaseCtx || firebaseCtx.initialising ? (
    <div id="loader">
      <CircularProgress />
    </div>
  ) : (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Switch>
          {/* <Route exact path="/" component={HomePage} /> */}
          <Route
            exact
            path="/test"
            // TEST SETTING
            component={MyForm}
          />
          <Route
            exact
            path="/"
            // TEST SETTING
            render={() => <Redirect to="/dashboard" />}
            // component={HomePage}
          />
          <Route
            exact
            path="/login"
            render={routerProps => (
              <Login {...routerProps} login={firebaseCtx.login} />
            )}
          />
          {/* <Route exact path="/register" component={Register} /> */}
          <PrivateRoute
            exact
            path="/dashboard"
            // render={routerProps => (
            //   <Dashboard {...routerProps} user={props.user} />
            // )}
            component={Dashboard}
            user={firebaseCtx.user}
          />
        </Switch>
      </Router>
    </MuiThemeProvider>
  );
}
