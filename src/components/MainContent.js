import React from "react";
import AdsPage from "./Ads/AdsPage";
import firebaseApp from "../auth/fireApi";
import MiniDrawer from "./SideDrawer/MiniDrawer";

class MainContent extends React.Component {
  /**
  |--------------------------------------------------
  | main content to show ads
  |--------------------------------------------------
  */
  constructor(props) {
    super(props);
    this.state = {
      ads: [],
      currentPage: null,
      isLoading: false,
      isFirebase: true,
      isSettingsModalOpen: false,
      distanceLimit: 50,
      isDistanceEnabled: true,
      user: this.props.me,
      userSettings: this.props.userSettings,

      // currently hardwire but might change to user setting
      filteredAds: [],
      currentCoords: ["45.4905074", "-73.58108349999999"],
      isTitleFilterEnabled: true,
      isSummaryFilterEnabled: false,
      isPathFilterEnabled: true
    };
    this.handleMessage = this.handleMessage.bind(this);
    this.handleSettingsClick = this.handleSettingsClick.bind(this);
    this.handleFormInputChange = this.handleFormInputChange.bind(this);
    this.handleDistanceSwitch = this.handleDistanceSwitch.bind(this);
  }

  distance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 === lat2 && lon1 === lon2) {
      return 0;
    } else {
      var radlat1 = (Math.PI * lat1) / 180;
      var radlat2 = (Math.PI * lat2) / 180;
      var theta = lon1 - lon2;
      var radtheta = (Math.PI * theta) / 180;
      var dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit === "K") {
        dist = dist * 1.609344;
      }
      if (unit === "N") {
        dist = dist * 0.8684;
      }
      return dist;
    }
  }

  // geo distance how far user is from ad poster
  parse_distance(entry) {
    let ad_lat = entry["geo_lat"];
    let ad_long = entry["geo_long"];
    let currentCoords = this.state.currentCoords;
    if (ad_lat && ad_long && currentCoords) {
      const distance = this.distance(
        parseFloat(ad_lat),
        parseFloat(ad_long),
        parseFloat(currentCoords[0]),
        parseFloat(currentCoords[1]),
        "K"
      );
      return Math.round(distance);
    } else {
      return -1;
    }
  }

  addAppPropertiesTo(ads) {
    let newArray = [];
    for (var i = 0; i < ads.length; i++) {
      let entry = ads[i];
      let distance = this.parse_distance(entry);
      let foo = {
        distance: distance,
        isNew: true
      };
      let goo = { ...foo, ...entry.appProperties };
      entry.appProperties = goo;
      newArray.push(entry);
    }
    return newArray;
  }

  handleSettingsClick() {
    this.setState({ isSettingsModalOpen: true });
  }

  handleMessage(ads) {
    const processedAds = this.addAppPropertiesTo(ads);
    this.setState({
      ads: processedAds
    });
  }

  // loading indicator switch
  handleClickQuery = () => {
    clearTimeout(this.timer);

    if (this.state.query !== "idle") {
      this.setState({
        query: "idle"
      });
      return;
    }

    this.setState({
      query: "progress"
    });
    this.timer = setTimeout(() => {
      this.setState({
        query: "success"
      });
    }, 2000);
  };

  handleFormInputChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleDistanceSwitch(event, checked) {
    this.setState({ isDistanceEnabled: checked });
  }

  // filters for removing ads
  filterTitle(title, terms) {
    terms.map(term => title.indexOf(term) === -1);
  }

  filterPath(path, terms) {
    terms.map(term => path.indexOf(term) === -1);
  }

  filterAds(ads, query = {}) {
    var filteredAds = [];
    query = {
      exclude: {
        fromPath: ["kids", "enfant", "mountain", "clothes", "soulier"],
        fromTitle: [
          "bequille",
          "rack",
          "casque",
          "helmet",
          "roue",
          "wheel",
          "infinity",
          "girl",
          "support",
          "enfants",
          "enfant",
          "vetements",
          "single",
          "suspension",
          "Tacx",
          "soulier",
          "choice",
          "garde",
          "shoes",
          "trottinette",
          "super cycle",
          "child",
          "kids",
          "Remorque",
          "garçon",
          "Tricycle",
          "fille",
          "montagne",
          "bmx",
          "bonelli",
          "supercycle",
          "gator",
          "folding",
          "fat"
        ],
        fromSummary: [
          "girafe",
          "vtt",
          "junior",
          "montagne",
          "ccm",
          "sportek",
          "youth",
          "kids",
          "boys",
          "fille",
          "support",
          "casque",
          "vybe",
          "bmx",
          "enfant",
          "stationnaire",
          "fixie",
          "remorque",
          "tricycle",
          "next",
          "fat",
          "MINELLI",
          "souliers",
          "pliant",
          "folding",
          "suspension",
          "tandem",
          "stationary",
          "leader",
          "Schwinn",
          "+ tx",
          "electrique",
          "supercycle",
          "Cuvillier",
          "noelic",
          "ROOSEBOOM",
          "vetements",
          "huffy",
          "jeune",
          "pepebikeshop",
          "h2s1h4",
          "Trottinette",
          "ecovelo",
          "THULE",
          "trailer",
          "protour",
          "nakamura",
          "ado",
          "zoombicycles",
          "Bike Guy",
          "cruiser",
          "infinity",
          "super cycle",
          "bonelli",
          "garçon"
        ]
      }
    };

    // bundles all the filters with boolean settings if they are enabled
    function filterClear(that, ad, query) {
      var result = [
        !that.state.isDistanceEnabled ||
          isWithinDistance(ad.distance, that.state.distanceLimit),
        !that.state.isPathFilterEnabled ||
          allGood(ad.link, query.exclude.fromPath),
        !that.state.isTitleFilterEnabled ||
          allGood(ad.title, query.exclude.fromTitle),
        !that.state.isSummaryFilterEnabled ||
          allGood(ad.summary, query.exclude.fromSummary)
      ].every(el => el === true);
      return result;
    }

    function isWithinDistance(distance, distanceLimit) {
      if (distance === -1) {
        return true;
      } else {
        const limit = distanceLimit;
        return distance <= limit;
      }
    }

    function allGood(stringToSearchIn, terms) {
      stringToSearchIn = stringToSearchIn.toLowerCase();
      return !terms.some(
        term => stringToSearchIn.indexOf(term.toLowerCase()) !== -1
      );
    }
    var that = this;
    filteredAds = ads.filter(ad => filterClear(that, ad, query));

    return filteredAds;
  }

  // firebase get of ads data
  getAds = () => {
    var database = firebaseApp.database();

    // TEST SETTING REMOVE should retriev this key from user settings
    var feedKey = "-Lg8Wg1v1rl9-i-i_O5U";
    let adsRef = database.ref("ads/" + feedKey).limitToLast(100);
    adsRef.on("value", snapshot => {
      const ads = snapshot.val();
      let newAds = [];
      for (const [adKey, adData] of Object.entries(ads)) {
        // add custom keys for this app to ad original data
        let distance = this.parse_distance(adData);
        adData["distance"] = distance;
        adData["fb_key"] = adKey;
        adData["feed_key"] = feedKey;

        newAds.unshift(adData);
      }
      this.setState({ ads: newAds });
    });
    adsRef.on("child_removed", function(data) {});
  };

  getUserSettings() {
    var settings = {};
    const myRef = firebaseApp.database().ref("users/" + this.state.user.uid);
    myRef
      .once("value", snap => {
        settings = snap.val();
      })
      .then(() => {
        this.setState({ userSettings: settings });
      });
  }

  componentDidMount() {
    // this.getUserSettings();
    this.getAds();
  }

  render() {
    const filteredAds = this.filterAds(this.state.ads);
    return (
      <React.Fragment>
        <MiniDrawer
          history={this.props.history}
          user={this.state.user}
          userSettings={this.state.userSettings}
          onLogoutClick={this.onLogoutClick}
          handleFormInputChange={this.handleFormInputChange}
          handleDistanceSwitch={this.handleDistanceSwitch}
          distanceLimit={this.state.distanceLimit}
          isDistanceEnabled={this.state.isDistanceEnabled}
          handleSettingsClick={this.handleSettingsClick}
          content=<AdsPage
            ads={filteredAds}
            isLoading={this.state.isLoading}
            currentCoords={this.state.currentCoords}
          />
        />
      </React.Fragment>
    );
  }
}

export default MainContent;
