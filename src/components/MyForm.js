import React, { Component } from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import LocationIcon from "@material-ui/icons/LocationOn";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import {
  LinearProgress,
  MenuItem,
  FormControl,
  InputLabel,
  FormControlLabel
} from "@material-ui/core";
import {
  fieldToTextField,
  TextField,
  TextFieldProps,
  Select,
  Switch
} from "formik-material-ui";
import FormWrapper from "./Form/FormWrapper";

const validationSchema = Yup.object().shape({
  username: Yup.string().required()
});

const userSettings = {
  distanceLimit: 50,
  email: "test@gmail.com",
  firstName: "Test",
  geoLat: 45.487718400000006,
  geoLong: -73.5748096,
  isDistanceEnabled: true,
  isPathFilterEnabled: false,
  isSummaryFilterEnabled: false,
  isTitleFitlerEnabled: false,
  lastName: "Guest",
  priceMax: 800,
  priceMin: 0,
  subscribedFeeds: {
    "-Lg8Wg1v1rl9-i-i_O5U": {
      desc: "Bikes from all of Canada Kijiji",
      exclFromPath: ["kids", "enfant", "mountain", "clothes", "soulier"],
      exclFromSummary: ["girafe", "vtt", "junior"],
      exclFromTitle: ["bequille", "rack"],
      feedKey: "-Lg8Wg1v1rl9-i-i_O5U",
      feedUid: "-Lg8Wg1v1rl9-i-i_O5U",
      inclInTitle: [""],
      name: "Bikes",
      url: "oijwoeifjlksjf.com"
    }
  }
};

const initialValues = {
  ...userSettings
};

const ranges = [
  {
    value: "none",
    label: "None"
  },
  {
    value: "0-20",
    label: "0 to 20"
  },
  {
    value: "21-50",
    label: "21 to 50"
  },
  {
    value: "51-100",
    label: "51 to 100"
  }
];

class MyForm extends Component {
  render() {
    return (
      <Formik
        initialValues={{
          ...userSettings,
          email: "",
          password: "",
          select: "none",
          tags: [],
          rememberMe: true
        }}
        validate={values => {}}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            setSubmitting(false);
            alert(JSON.stringify(values, null, 2));
          }, 500);
        }}
        render={({ submitForm, isSubmitting, values, setFieldValue }) => (
          <Form>
            <Field
              type="password"
              label="Password"
              name="password"
              component={TextField}
            />
            <br />
            <FormControlLabel
              control={
                <Field
                  label="Remember Me"
                  name="rememberMe"
                  component={Switch}
                />
              }
              label="Remember Me"
            />
            <br />
            <Field
              type="text"
              name="select"
              label="With Select"
              select
              helperText="Please select Range"
              margin="normal"
              component={TextField}
              InputLabelProps={{
                shrink: true
              }}
            >
              {ranges.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </Field>
            <br />
            <FormControl>
              <InputLabel shrink={true} htmlFor="tags">
                Tags
              </InputLabel>
              <Field
                type="text"
                name="tags"
                component={Select}
                multiple={true}
                inputProps={{ name: "tags", id: "tags" }}
              >
                <MenuItem value="dogs">Dogs</MenuItem>
                <MenuItem value="cats">Cats</MenuItem>
                <MenuItem value="rats">Rats</MenuItem>
                <MenuItem value="snakes">Snakes</MenuItem>
              </Field>
            </FormControl>
            <br />
            {isSubmitting && <LinearProgress />}
            <br />
            <Button
              variant="outlined"
              color="primary"
              disabled={isSubmitting}
              onClick={submitForm}
            >
              Submit
            </Button>
          </Form>
        )}
      />
    );
  }
}

export default MyForm;
