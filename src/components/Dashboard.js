import React, { useEffect, useState, useCallback } from "react";
import { Typography, Paper, Container } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";
import { withRouter } from "react-router-dom";
import Gallery from "./Ads/Gallery";
import SideDrawer from "./SideDrawer/MiniDrawer";
import ContentCtx from "../contexts/content-context";
import AppCtx from "../contexts/app-context";

// import { FirebaseCtxConsumer } from "../contexts/firebase-context";

const styles = theme => ({
  dashboard: { display: "flex" },
  main: {
    display: "flex"
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`
  },
  uiContainer: {
    display: "flex"
  },
  avatar: {
    margin: theme.spacing(),
    backgroundColor: theme.palette.secondary.main
  },
  submit: {
    marginTop: theme.spacing(3)
  },
  content: {
    flexGrow: 1,
    paddingTop: "10px",
    paddingRight: "2px",
    paddingBottom: "20px"
  },
  adsPaper: {
    flexGrow: 1,
    margin: "0px 0px 0px 0px",
    padding: "5px",
    minHeight: "33%"
  }
});

// main app ui
function Dashboard(props) {
  const { classes } = props;
  const userCtx = React.useContext(AppCtx);
  const contentCtx = React.useContext(ContentCtx);
  const [userSettings, setUserSettings] = useState(false);
  const [filteredAds, setFilteredAds] = useState([]);
  const [globalFilters, setGlobalFilters] = useState({});
  const [distanceLimit, setDistanceLimit] = useState(null);
  const [minPrice, setMinPrice] = useState(null);
  const [maxPrice, setMaxPrice] = useState(null);
  const [isDistanceEnabled, setIsDistanceEnabled] = useState(null);
  const [isFiltersChanged, setIsFiltersChanged] = useState(false);
  const [lastAdKey, setLastAdKey] = useState(null);

  // side drawer
  const [open, setOpen] = React.useState(true);

  useEffect(() => {
    if (userCtx) {
      setUserSettings({ ...userCtx.userSettings });
      setGlobalFilters({
        distanceLimit: userCtx.userSettings.distanceLimit,
        isDistanceEnabled: userCtx.userSettings.isDistanceEnabled
      });
      setDistanceLimit(userCtx.userSettings.distanceLimit);
      setIsDistanceEnabled(userCtx.userSettings.isDistanceEnabled);
      setMinPrice(userCtx.userSettings.minPrice);
      setMaxPrice(userCtx.userSettings.maxPrice);
      setIsFiltersChanged(true);
    }
  }, [userCtx]);

  const filterCallback = useCallback(
    newAds => {
      function filterAds(ads, query = {}) {
        let adsCollector = [];
        query = {
          exclude: {
            fromPath: ["kids", "enfant", "mountain", "clothes", "soulier"],
            fromTitle: [],
            fromSummary: []
          }
        };

        function isWithinDistance(distance, distanceLimit) {
          if (distance === -1) {
            return true;
          }
          const limit = distanceLimit;
          return distance <= limit;
        }

        function isWithinPrice(price, minPrice, maxPrice) {
          if (!price && !minPrice) {
            return true;
          }

          return minPrice <= price && price <= maxPrice;
        }

        function allGood(text, terms) {
          const stringToSearchIn = text.toLowerCase();
          return !terms.some(
            term => stringToSearchIn.indexOf(term.toLowerCase()) !== -1
          );
        }
        // bundles all the filters with boolean settings if they are enabled
        function filterClear(that, ad, query) {
          const result = [
            !userSettings.isDistanceEnabled ||
              isWithinDistance(ad.distance, userSettings.distanceLimit),
            isWithinPrice(
              ad["g-core_price"],
              userSettings.minPrice,
              userSettings.maxPrice
            ),
            !userSettings.isPathFilterEnabled ||
              allGood(ad.link, query.exclude.fromPath),
            !userSettings.isTitleFilterEnabled ||
              allGood(ad.title, query.exclude.fromTitle),
            !userSettings.isSummaryFilterEnabled ||
              allGood(ad.summary, query.exclude.fromSummary)
          ].every(el => el === true);
          return result;
        }
        const that = this;
        adsCollector = ads.filter(ad => filterClear(that, ad, query));

        return adsCollector;
      }

      return filterAds(newAds);
      // setFilteredAds(filteredAds => newFilteredAds.concat(filteredAds));
    },
    [
      userSettings.distanceLimit,
      userSettings.isDistanceEnabled,
      userSettings.isPathFilterEnabled,
      userSettings.isSummaryFilterEnabled,
      userSettings.isTitleFilterEnabled,
      userSettings.maxPrice,
      userSettings.minPrice
    ]
  );

  useEffect(() => {
    if (isFiltersChanged) {
      setIsFiltersChanged(false);
      const resetAds = filterCallback(contentCtx);
      setFilteredAds(resetAds);
    } else {
      const currentAd = contentCtx[0];
      if (currentAd && currentAd.adKey !== lastAdKey) {
        setLastAdKey(currentAd.adKey);
        const newFilteredAds = filterCallback([currentAd]);
        setFilteredAds(prevAds => [...newFilteredAds, ...prevAds]);
      }
    }
  }, [contentCtx, filterCallback, isFiltersChanged, lastAdKey]);

  let timeout;

  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }

  const toggleDrawer = () => {
    setOpen(!open);
  };

  useEffect(() => {
    if (userCtx) {
      setUserSettings({ ...userCtx.userSettings });
    }
  }, [userCtx]);

  const updateField = e => {
    setUserSettings({
      ...userSettings,
      [e.target.name]: e.target.value
    });
  };

  function saveToFirebase() {
    alert("saving");
  }

  const handleFormChange = e => {
    clearTimeout(timeout);
    const newUserSettings = {
      ...userSettings,
      [e.target.name]: e.target.value
    };
    setUserSettings(newUserSettings);
    timeout = setTimeout(saveToFirebase, 5000);
  };

  return (
    <div className={classes.dashboard}>
      <SideDrawer
        open={open}
        handleDrawerClose={handleDrawerClose}
        handleDrawerOpen={handleDrawerOpen}
        toggleDrawer={toggleDrawer}
        userSettings={userSettings}
      />
      <main className={classes.content}>
        <Paper className={classes.adsPaper}>
          {/* {JSON.stringify(ads)} */}
          {/* <Typography>{JSON.stringify(filteredAds)}</Typography> */}
          {/* <Typography>{JSON.stringify(contentCtx)}</Typography> */}
          <Gallery elements={filteredAds} />
          {/* <Typography>{JSON.stringify(userSettings)}</Typography> */}
        </Paper>
      </main>
    </div>
  );
}

export default withRouter(withStyles(styles)(Dashboard));
