import React, { Component } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { FormikTextField } from "formik-material-fields";

const validationSchema = Yup.object().shape({
  username: Yup.string().required()
});

const initialValues = {
  username: ""
};

class MyForm extends Component {
  render() {
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={this.props.onSubmit}
      >
        {({ isValid }) => (
          <Form autoComplete="off">
            <FormikTextField
              name="username"
              label="Username"
              margin="normal"
              fullWidth
            />
          </Form>
        )}
      </Formik>
    );
  }
}
