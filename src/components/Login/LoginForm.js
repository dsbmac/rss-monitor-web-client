import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";

class LoginForm extends React.Component {
  /**
  |--------------------------------------------------
  | Can login with either Google Auth or email password
  | form
  |--------------------------------------------------
  */
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      submitting: false
    };
  }

  handleGoogleClick = e => {
    const { onGoogle } = this.props;
    if (onGoogle) {
      onGoogle();
    }
  };

  handleSubmit = e => {
    e.preventDefault();
    const { onSubmit } = this.props;
    const { email, password } = this.state;
    if (onSubmit) {
      this.setState({ submitting: true });
      onSubmit(email, password);
    }
  };

  // form state helper
  handleChange = key => e => {
    this.setState({ [key]: e.target.value });
  };

  render() {
    const { email, password, submitting } = this.state;
    return (
      <React.Fragment>
        <Button
          style={{ marginBottom: 24 }}
          fullWidth
          variant={"contained"}
          color={"primary"}
          onClick={this.handleGoogleClick}
        >
          Continue with Google
        </Button>
        <Divider variant="middle" style={{ marginBottom: 24 }} />
        <form
          style={{ display: "flex", flexDirection: "column" }}
          onSubmit={this.handleSubmit}
        >
          <TextField
            style={{ marginBottom: 24 }}
            variant={"outlined"}
            required
            type={"email"}
            label={"Email"}
            value={email}
            onChange={this.handleChange("email")}
          />
          <TextField
            style={{ marginBottom: 24 }}
            variant={"outlined"}
            required
            type={"password"}
            label={"Password"}
            value={password}
            onChange={this.handleChange("password")}
          />
          <Button
            type={"submit"}
            fullWidth
            variant={"contained"}
            color={"primary"}
          >
            {submitting ? (
              <CircularProgress
                style={{ color: "#fff" }}
                color={"inherit"}
                size={16}
              />
            ) : (
              "Submit"
            )}
          </Button>
        </form>
      </React.Fragment>
    );
  }
}

export default LoginForm;
