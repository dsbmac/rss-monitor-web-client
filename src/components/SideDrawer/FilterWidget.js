import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import FilterIcon from "@material-ui/icons/FilterList";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FilledInput from "@material-ui/core/FilledInput";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import SideForm from "./SideForm";
import TermsArray from "./TermsArray";
import { Container } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  feedForm: {
    display: "flex",
    justifyContent: "center",
    marginTop: -15,
    paddingLeft: 20
  },
  termsContainer: {
    display: "flex",
    flexWrap: "wrap"
  }
}));

function FilterWidget(props) {
  const classes = useStyles();
  const [feeds, setFeeds] = useState(props.feeds);
  const [selectedfeed, setSelectedFeed] = useState(false);
  const [terms, setTerms] = useState([]);

  useEffect(() => {
    return () => {
      setFeeds(props.feeds);
      if (feeds.length > 0) {
        setTerms(feeds[0][1].filters.exclude.fromTitle);
      }
    };
  }, [feeds, feeds.length, props]);
  const handleInputChange = e => {
  };

  return (
    <div>
      <ListItem key="filters">
        <ListItemIcon button="true" onClick={props.toggleDrawer}>
          {<FilterIcon />}
        </ListItemIcon>
        <FormControl margin="dense">
          <InputLabel>Category</InputLabel>
          <Select
            disabled
            value="Bikes"
            inputProps={{
              name: "category",
              id: "category-simple"
            }}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value="Bikes">Bikes</MenuItem>
          </Select>
        </FormControl>
      </ListItem>
      <Container className={classes.feedForm}>
        <SideForm handleInputChange={handleInputChange} />
      </Container>
      <Container className={classes.termsContainer}>
        <TermsArray terms={terms} />
      </Container>
    </div>
  );
}

export default FilterWidget;
