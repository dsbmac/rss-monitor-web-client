import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    padding: theme.spacing(0.5)
  },
  termChip: {
    margin: theme.spacing(0.2)
  }
}));

function TermsArray(props) {
  const classes = useStyles();
  const [chipData, setChipData] = React.useState([]);

  useEffect(() => {
    return () => {
      setChipData(props.terms);
    };
  }, [props]);

  const handleDelete = data => () => {
    if (data.label === "React") {
      alert("Why would you want to delete React?! :)");
      return;
    }

    const chipToDelete = chipData.indexOf(data);
    chipData.splice(chipToDelete, 1);
    setChipData(chipData);
  };

  return (
    <React.Fragment>
      {chipData.map(data => {
        return (
          <Chip
            key={data}
            label={data}
            onDelete={handleDelete(data)}
            size="small"
            className={classes.termChip}
          />
        );
      })}
    </React.Fragment>
  );
}

export default TermsArray;
