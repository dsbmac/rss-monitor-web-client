import React, { useState, useEffect } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import FeedFilter from "./FeedFilter";
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: theme.spacing(4)
  }
}));

function FiltersList(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(props.isOpen);
  const [feeds, setFeeds] = useState(props.feeds);
  useEffect(() => {
    return () => {
      setFeeds(props.feeds);
    };
  }, [props]);

  function handleClick() {
    setOpen(!open);
  }

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      {feeds.map(feed => {
        return <FeedFilter {...feed} key={feed.feedUid} />;
      })}
    </List>
  );
}

export default FiltersList;

function FiltersExpansionPanel(props) {
  const classes = useStyles();
  const [feeds, setFeeds] = useState(props.feeds);
  useEffect(() => {
    return () => {
      setFeeds(props.feeds);
    };
  }, [props.feeds]);

  return (
    <div className={classes.root}>
      {feeds.map(feed => {
        return (
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>{feed.name}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <FeedFilter {...feed} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
        );
      })}
    </div>
  );
}

function Filters(props) {
  const [feeds, setFeeds] = useState(props.feeds);
  useEffect(() => {
    return () => {
      setFeeds(props.feeds);
    };
  }, [props]);

  return (
    <List>
      {feeds.map(feed => {
        return (
          <ListItem key={feed.feedUid}>
            <FeedFilter {...feed} />
          </ListItem>
        );
      })}
    </List>
  );
}
