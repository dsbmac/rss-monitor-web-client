import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Formik, Field } from "formik";
import { TextField, Switch } from "formik-material-ui";
import * as Yup from "yup";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import LocationIcon from "@material-ui/icons/LocationOn";
import InputAdornment from "@material-ui/core/InputAdornment";
import PersonIcon from "@material-ui/icons/Person";
import FilterIcon from "@material-ui/icons/FilterList";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import Toolbar from "@material-ui/core/Toolbar";
import { Divider } from "@material-ui/core";
import FirebaseCtx from "../Firebase/context";

const useStyles = makeStyles(theme => ({
  hide: {
    display: "none"
  }
}));

const validationSchema = Yup.object().shape({
  distanceLimit: Yup.number()
    .positive("Greater than zero nitwit")
    .integer(),
  minPrice: Yup.number()
    .integer()
    .positive()
    .test("passwords-match", "Passwords must match ya fool", function(value) {
      return value < this.parent.maxPrice;
    }),
  maxPrice: Yup.number()
    .integer()
    .positive(),
  isDistanceEnabled: Yup.boolean()
});

const testUserSettings = {
  distanceLimit: 50,
  email: "test@gmail.com",
  firstName: "Test",
  geoLat: 45.487718400000006,
  geoLong: -73.5748096,
  isDistanceEnabled: true,
  isPathFilterEnabled: false,
  isSummaryFilterEnabled: false,
  isTitleFitlerEnabled: false,
  lastName: "Guest",
  priceMax: 800,
  priceMin: 0,
  subscribedFeeds: {
    "-Lg8Wg1v1rl9-i-i_O5U": {
      desc: "Bikes from all of Canada Kijiji",
      exclFromPath: ["kids", "enfant", "mountain", "clothes", "soulier"],
      exclFromSummary: ["girafe", "vtt", "junior"],
      exclFromTitle: ["bequille", "rack"],
      feedKey: "-Lg8Wg1v1rl9-i-i_O5U",
      feedUid: "-Lg8Wg1v1rl9-i-i_O5U",
      inclInTitle: [""],
      name: "Bikes",
      url: "oijwoeifjlksjf.com"
    }
  }
};

// function SideForm(props) {
//   const data = props.formData;
//   return data ? (
//     <Formik
//       initialValues={data}
//       enableReinitialize
//       validate={validationSchema}
//       onSubmit={(values, actions) => {
//         setTimeout(() => {
//           alert(JSON.stringify(values, null, 2));
//           actions.setSubmitting(false);
//         }, 1000);
//       }}
//     >
//       {props => {
//         const {
//           values,
//           touched,
//           errors,
//           dirty,
//           isSubmitting,
//           handleChange,
//           handleBlur,
//           handleSubmit,
//           handleReset,
//           isValid
//         } = props;
//         return (
//           <Form autoComplete="off">
//             <List>
//               <div style={{ overflowWrap: "normal" }}>
//                 {JSON.stringify(props.isValid, null, 2)}
//               </div>
//               <ListItem key={"distance"}>
//                 <ListItemIcon button="true" onClick={props.toggleDrawer}>
//                   {<LocationIcon />}
//                 </ListItemIcon>
//                 <Field
//                   component={TextField}
//                   name="distanceLimit"
//                   placeholder="Enter Number"
//                   label="Distance"
//                   type="number"
//                   InputLabelProps={{
//                     shrink: true
//                   }}
//                   margin="dense"
//                   InputProps={{
//                     endAdornment: (
//                       <InputAdornment position="end">km</InputAdornment>
//                     )
//                   }}
//                 />
//                 <Field
//                   component={Switch}
//                   color="primary"
//                   name="isDistanceEnabled"
//                 />
//               </ListItem>
//               <ListItem>
//                 <ListItemIcon button="true" onClick={props.toggleDrawer}>
//                   {<AttachMoneyIcon />}
//                 </ListItemIcon>
//                 <Field
//                   component={TextField}
//                   name="minPrice"
//                   label="Price"
//                   type="number"
//                   //   className={classes.textField}
//                   InputLabelProps={{
//                     shrink: true
//                   }}
//                   margin="dense"
//                   helperText="min"
//                 />
//                 <div style={{ margin: "-5px 10px 0px 10px" }}>_</div>
//                 <Field
//                   component={TextField}
//                   label=" "
//                   name="maxPrice"
//                   type="number"
//                   InputLabelProps={{
//                     shrink: true
//                   }}
//                   margin="dense"
//                   helperText="max"
//                 />
//               </ListItem>
//               <Toolbar style={{ justifyContent: "flex-end" }}>
//                 <Button>Cancel</Button>
//                 <Button color="primary">Save</Button>
//               </Toolbar>
//               <ListItem key="filters" button disabled>
//                 <ListItemIcon button="true" onClick={props.toggleDrawer}>
//                   {<FilterIcon />}
//                 </ListItemIcon>
//                 <ListItemText>Text Filters</ListItemText>
//               </ListItem>
//             </List>
//           </Form>
//         );
//       }}
//     </Formik>
//   ) : null;
// }
const DisplayFormikState = props => (
  <div style={{ margin: "1rem 0" }}>
    <h3 style={{ fontFamily: "monospace" }} />
    <pre
      style={{
        background: "#f6f8fa",
        fontSize: ".65rem",
        padding: ".5rem"
      }}
    >
      <strong>props</strong> = {JSON.stringify(props, null, 2)}
    </pre>
  </div>
);

function SideForm(props) {
  const firebaseCtx = React.useContext(FirebaseCtx);
  const [isSideDrawerOpen, setIsSideDrawerOpen] = useState(
    props.isSideDrawerOpen
  );
  const classes = useStyles();
  const userSettings = props.formData;
  useEffect(() => {
    setIsSideDrawerOpen(props.isSideDrawerOpen);
  }, [props.isSideDrawerOpen]);

  const handleLogout = () => {
    console.log("lgging out");
    firebaseCtx.logout();
  };

  return userSettings ? (
    <Formik
      initialValues={userSettings}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          firebaseCtx.saveUserSettings(values);
          // alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 500);
      }}
      validationSchema={Yup.object().shape({
        distanceLimit: Yup.number()
          .positive("must be positive")
          .integer("No decimals")
          .max(999, "too large"),
        isDistanceEnabled: Yup.boolean(),
        minPrice: Yup.number()
          .integer()
          .max(99999999, "too large")
          .test("less than", "is > max", function(value) {
            return value < this.parent.maxPrice && value >= 0;
          }),
        maxPrice: Yup.number()
          .integer()
          .positive()
          .max(99999999, "too large")
          .test("greater than", "is < min", function(value) {
            return value && value > this.parent.minPrice;
          })
      })}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          dirty,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
          resetForm
        } = props;
        return (
          <form
            onSubmit={e => {
              handleSubmit(e);
              resetForm(values);
            }}
          >
            <List>
              <ListItem key="distance">
                <ListItemIcon button="true" onClick={props.toggleDrawer}>
                  {<LocationIcon />}
                </ListItemIcon>
                <Field
                  id="distanceLimit"
                  component={TextField}
                  name="distanceLimit"
                  placeholder="Enter Number"
                  label="Distance"
                  type="number"
                  InputLabelProps={{
                    shrink: true
                  }}
                  margin="dense"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">km</InputAdornment>
                    )
                  }}
                  onBlur={handleBlur}
                />
                <Field
                  component={Switch}
                  color="primary"
                  name="isDistanceEnabled"
                  margin="dense"
                />
              </ListItem>
              <ListItem>
                <ListItemIcon button="true" onClick={props.toggleDrawer}>
                  {<AttachMoneyIcon />}
                </ListItemIcon>
                <Field
                  component={TextField}
                  name="minPrice"
                  label="Price"
                  type="number"
                  //   className={classes.textField}
                  InputLabelProps={{
                    shrink: true
                  }}
                  margin="dense"
                  helperText="min"
                />
                <div style={{ margin: "-8px 10px 0px 10px" }}>_</div>
                <Field
                  component={TextField}
                  label=" "
                  name="maxPrice"
                  type="number"
                  InputLabelProps={{
                    shrink: true
                  }}
                  margin="dense"
                  helperText="max"
                />
              </ListItem>
              {isSideDrawerOpen ? (
                <Toolbar style={{ justifyContent: "flex-end" }}>
                  <Button
                    onClick={handleReset}
                    disabled={!dirty || isSubmitting}
                  >
                    Cancel
                  </Button>
                  <Button
                    type="submit"
                    color="primary"
                    disabled={isSubmitting || !props.isValid}
                  >
                    Save
                  </Button>
                </Toolbar>
              ) : null}
              <Divider />
              <ListItem key="avatar">
                <ListItemIcon button="true" onClick={props.toggleDrawer}>
                  {<PersonIcon />}
                </ListItemIcon>
                <ListItemText>Profile</ListItemText>
                <Button variant="contained" size="small" onClick={handleLogout}>
                  Logout
                </Button>
              </ListItem>
            </List>
          </form>
        );
      }}
    </Formik>
  ) : null;
}

export default SideForm;
