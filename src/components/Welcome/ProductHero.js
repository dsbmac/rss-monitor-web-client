import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ProductHeroLayout from './ProductHeroLayout';
// 'https://images.unsplash.com/photo-1534854638093-bada1813ca19?auto=format&fit=crop&w=1400&q=80';
const backgroundImage =
    'https://firebasestorage.googleapis.com/v0/b/react-firebase-f1903.appspot.com/o/BeFunky%20Collage.jpg?alt=media&token=8970872a-f4f0-43fb-bce0-d49ea0ccd539';

const styles = theme => ({
    background: {
        backgroundImage: `url(${backgroundImage})`,
        backgroundColor: '#7fc7d9', // Average color of the background image.
        backgroundPosition: 'center',
    },
    button: {
        minWidth: 200,
    },
    h5: {
        marginBottom: theme.spacing(4),
        marginTop: theme.spacing(4),
        [theme.breakpoints.up('sm')]: {
            marginTop: theme.spacing(10),
        },
    },
    more: {
        marginTop: theme.spacing(2),
    },
});

function ProductHero(props) {
    const { classes } = props;

    return (
        <ProductHeroLayout backgroundClassName={classes.background}>
            {/* Increase the network loading priority of the background image. */}
            <img style={{ display: 'none' }} src={backgroundImage} alt="" />
            <Typography color="inherit" align="center" variant="h2" marked="center">
                Real-time classifieds ads
            </Typography>
            <Typography color="inherit" align="center" variant="h5" className={classes.h5}>
                Track Kijiji, Craigslist ads with powerful filters and real time updates.
      </Typography>
            <Button
                color="secondary"
                variant="contained"
                size="large"
                className={classes.button}
                component="a"
                href="/signup/"
            >
                Register
      </Button>
            <Typography variant="body2" color="inherit" className={classes.more}>
                Find stuff fast
      </Typography>
        </ProductHeroLayout>
    );
}

ProductHero.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductHero);