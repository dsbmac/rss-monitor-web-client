import app from "firebase/app";
import "firebase/auth";
import "firebase/database";

const config = {
  apiKey: "AIzaSyDzN0n7DAd3jdv8a0JfD5duVWfL6qhy-BA",
  authDomain: "rss-monitor.firebaseapp.com",
  databaseURL: "https://rss-monitor.firebaseio.com",
  projectId: "rss-monitor",
  storageBucket: "rss-monitor.appspot.com",
  messagingSenderId: "981457092013",
  appId: "1:981457092013:web:ab9283ddfec6f07a"
};

class Firebase {
  constructor() {
    app.initializeApp(config);
  }
}

export default Firebase;

// class Firebase {
//   constructor() {
//     app.initializeApp(config);
//     this.auth = app.auth();
//     this.db = app.database();
//   }

//   login(email, password) {
//     return this.auth.signInWithEmailAndPassword(email, password);
//   }

//   logout() {
//     return this.auth.signOut();
//   }

//   async register(name, email, password) {
//     await this.auth.createUserWithEmailAndPassword(email, password);
//     return this.auth.currentUser.updateProfile({
//       displayName: name
//     });
//   }

//   //   addQuote(quote) {
//   //     if (!this.auth.currentUser) {
//   //       return alert("Not authorized");
//   //     }

//   //     return this.db
//   //       .doc(`users_codedamn_video/${this.auth.currentUser.uid}`)
//   //       .set({
//   //         quote
//   //       });
//   //   }

//   isInitialized() {
//     return new Promise(resolve => {
//       this.auth.onAuthStateChanged(resolve);
//     });
//   }
//   isUserInitialized() {
//     return new Promise(resolve => {
//       this.auth.onAuthStateChanged(resolve);
//     });
//   }

//   getUserId() {
//     return this.auth.currentUser && this.auth.currentUser.uid;
//   }
//   getCurrentUser() {
//     return this.auth.currentUser && this.auth.currentUser;
//   }

//   getCurrentUsername() {
//     return this.auth.currentUser && this.auth.currentUser.uid;
//   }

//   getUserSettings() {
//     var settings = {};
//     // TEST setting
//     const uid = "CLk1BWzkqHfaHmwe1uzmYyocal03";
//     const myRef = this.db.ref("users/" + uid);
//     return myRef.once("value");
//   }

//   adsRef() {
//     // TEST SETTING REMOVE should retriev this key from user settings
//     var feedKey = "-Lg8Wg1v1rl9-i-i_O5U";
//     let adsRef = this.db.ref("ads/" + feedKey).limitToLast(100);
//     return adsRef;
//   }

//   writeUserData(userId, formData) {
//     this.db.ref("users/" + this.auth.uid).set(formData);
//   }
//   //   async getCurrentUserQuote() {
//   //     const quote = await this.db
//   //       .doc(`users_codedamn_video/${this.auth.currentUser.uid}`)
//   //       .get();
//   //     return quote.get("quote");
//   //   }
// }

// export default new Firebase();
