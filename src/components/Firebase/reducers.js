export const LOGIN = "LOGIN";
export const REMOVE_PRODUCT_FROM_CART = "REMOVE_PRODUCT_FROM_CART";

function login() {}

function shopReducer(state, action) {
  switch (action.type) {
    case LOGIN:
      return login(state);
    default:
      return state;
  }
}

export default shopReducer;
