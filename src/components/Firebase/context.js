import React, { useCallback, useEffect, createContext, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import firebaseApp, { fireAuth } from "../../auth/fireApi";

const FirebaseCtx = createContext();

// make a functional comp return the provider

function FirebaseProvider(props) {
  const [user, initialising, error] = useAuthState(fireAuth);

  function login(email, password) {
    return fireAuth.signInWithEmailAndPassword(email, password);
  }

  function logout() {
    return fireAuth.signOut();
  }

  const saveUserSettings = useCallback(
    newUserSettings => {
      firebaseApp
        .database()
        .ref(`users/${user.uid}`)
        .set(newUserSettings);
    },
    [user]
  );

  const [value, setValue] = useState({
    user,
    initialising,
    login,
    logout,
    firebase: firebaseApp,
    saveUserSettings
  });

  useEffect(() => {
    setValue({
      user,
      initialising,
      login,
      logout,
      firebase: firebaseApp,
      saveUserSettings
    });
  }, [initialising, error, user, setValue, saveUserSettings]);

  return (
    <FirebaseCtx.Provider value={value}>{props.children}</FirebaseCtx.Provider>
  );
}

export { FirebaseProvider };
export default FirebaseCtx;
