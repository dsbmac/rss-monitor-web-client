import React from "react";
import AppCtx from "../contexts/app-context";
import ContentCtx from "../contexts/content-context";

function TestCtxComponent() {
  const context = React.useContext(AppCtx);
  const contentCtx = React.useContext(ContentCtx);
  const [feedUid, setFeedUid] = React.useState(context.feedUid);
  const [ads, setAds] = React.useState(contentCtx.ads);
  return <div>{JSON.stringify(contentCtx)}</div>;
}

export default TestCtxComponent;
