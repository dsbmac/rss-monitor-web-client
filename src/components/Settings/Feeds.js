import React from "react";
import PropTypes from "prop-types";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import InboxIcon from "@material-ui/icons/Inbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import ButtonBase from "@material-ui/core/ButtonBase";
import SimpleModal from "./SimpleModal";

const TEST_DATA = {
  feedKey: "-Lg8Wg1v1rl9-i-i_O5U",
  desc: "Bikes from all of Canada Kijiji",
  exclFromPath: ["kids", "enfant", "mountain", "clothes", "soulier"],
  exclFromSummary: [
    "girafe",
    "vtt",
    "junior",
    "montagne",
    "ccm",
    "sportek",
    "youth",
    "kids",
    "boys",
    "fille",
    "support",
    "casque",
    "vybe",
    "bmx",
    "enfant",
    "stationnaire",
    "fixie",
    "remorque",
    "tricycle",
    "next",
    "fat",
    "MINELLI",
    "souliers",
    "pliant",
    "folding",
    "suspension",
    "tandem",
    "stationary",
    "leader",
    "Schwinn",
    "+ tx",
    "electrique",
    "supercycle",
    "Cuvillier",
    "noelic",
    "ROOSEBOOM",
    "vetements",
    "huffy",
    "jeune",
    "pepebikeshop",
    "h2s1h4",
    "Trottinette",
    "ecovelo",
    "THULE",
    "trailer",
    "protour",
    "nakamura",
    "ado",
    "zoombicycles",
    "Bike Guy",
    "cruiser",
    "infinity",
    "super cycle",
    "bonelli",
    "garçon"
  ],
  exclFromTitle: [
    "bequille",
    "rack",
    "casque",
    "helmet",
    "roue",
    "wheel",
    "infinity",
    "girl",
    "support",
    "enfants",
    "enfant",
    "vetements",
    "single",
    "suspension",
    "Tacx",
    "soulier",
    "choice",
    "garde",
    "shoes",
    "trottinette",
    "super cycle",
    "child",
    "kids",
    "Remorque",
    "garçon",
    "Tricycle",
    "fille",
    "montagne",
    "bmx",
    "bonelli",
    "supercycle",
    "gator",
    "folding",
    "fat"
  ],
  feedUid: "-Lg8Wg1v1rl9-i-i_O5U",
  inclInTitle: [""],
  name: "bikes",
  url: "oijwoeifjlksjf.com"
};

const styles = theme => ({
  root: {
    width: "100%",
    maxWidth: 320,
    backgroundColor: theme.palette.background.paper
  }
});

const useStyles = makeStyles(theme => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(4),
    outline: "none"
  }
}));

function FeedCard(props) {
  return (
    <div
      onClick={evt => {
        props.handler(props.feedData);
      }}
    >
      <Card>
        <CardContent>
          <Typography variant="h5" component="h2">
            {props.name}
          </Typography>
          <Typography color="textSecondary">{props.url}</Typography>
          <Typography variant="body2" component="p">
            {props.desc}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}

function Feeds(props) {
  const { classes } = props;
  const [open, setOpen] = React.useState(true);
  const [feeds, setFeeds] = React.useState(feedsObjAsArray(props.feeds));
  const [selectedFeed, setSelectedFeed] = React.useState(TEST_DATA);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleOpenModal = feedData => {
    setSelectedFeed(feedData);
    setOpen(true);
  };

  function feedsObjAsArray(feeds) {
    var result = [];
    if (feeds) {
      return Object.entries(feeds);
    }
    return result;
  }

  return (
    <div className={classes.root}>
      <SimpleModal
        open={open}
        handleClose={handleClose}
        feedData={selectedFeed}
      />
      {feeds.map(feedEntry => {
        let feed = { feedKey: feedEntry[0], ...feedEntry[1] };
        return <FeedCard {...feed} handler={handleOpenModal} feedData={feed} />;
      })}
    </div>
  );
}

Feeds.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Feeds);
