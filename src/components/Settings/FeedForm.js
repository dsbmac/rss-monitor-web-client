import React, { useState } from "react";
import clsx from "clsx";
// import firebaseApp, { fireAuth } from "../../auth/fireApi";
// import firebase from "../../auth/firebase";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import SaveIcon from "@material-ui/icons/Save";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import MenuIcon from "@material-ui/icons/Menu";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";

const useStyles = makeStyles(theme => ({
  saveButtonGrid: {},
  toolbarButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px"
  },
  button: {
    margin: theme.spacing(1)
  },
  leftIcon: {
    marginRight: theme.spacing(1)
  },
  rightIcon: {
    marginLeft: theme.spacing(1)
  },
  iconSmall: {
    fontSize: 20
  },
  title: {
    flexGrow: 1
  }
}));

export default function FeedForm(props) {
  const classes = useStyles();

  const [feedKey, setFeedKey] = useState(props.feedKey);
  const [url, setUrl] = useState(props.url);
  const [name, setFeedName] = useState(props.name);
  const [desc, setDesc] = useState(props.desc);
  const [inclInTitle, setInclInTitle] = useState(props.inclInTitle);
  const [exclFromTitle, setExclFromTitle] = useState(props.exclFromTitle);

  const handleSubmit = () => {
    const formData = {
      name: name,
      desc: desc,
      inclInTitle: inclInTitle,
      exclFromTitle: exclFromTitle
    };

    const newData = { ...props.feedData, ...formData };
    // alert(JSON.stringify(newData));
    // saveToFirebase(fireAuth.currentUser.uid, newData);
  };

  // const saveToFirebase = (userId, formData) => {
  //   var database = firebaseApp.database();
  //   alert(feedKey);
  //   let myRef = database
  //     .ref("users/" + userId)
  //     .child("subscribedFeeds")
  //     .child(feedKey).ref;

  //   myRef.set(formData);
  // };

  return (
    <React.Fragment>
      <AppBar color="default" position="absolute">
        <Toolbar>
          <Typography
            component="h1"
            variant="h6"
            color="primary"
            noWrap
            className={classes.title}
          >
            Edit Feed
          </Typography>
          <Button
            variant="contained"
            color="inherit"
            className={classes.button}
            onClick={props.handleClose}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
          >
            <SaveIcon className={clsx(classes.leftIcon, classes.iconSmall)} />
            Save
          </Button>
        </Toolbar>
      </AppBar>
      <form>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <Typography variant="h5" id="feed-form-title">
              Edit Feed
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} />
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="name"
              name="name"
              label="Feed name"
              value={name}
              fullWidth
              helperText={url}
              onChange={e => setFeedName(e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="desc"
              name="desc"
              label="Description"
              fullWidth
              value={props.desc}
              onChange={e => setDesc(e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" id="filters-subtitle">
              Filters
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6}>
            Include
          </Grid>
          <Grid item xs={12} sm={6}>
            Exclude
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="inclInTitle"
              name="inclInTitle"
              helperText="In Title"
              variant="outlined"
              fullWidth
              margin="dense"
              onChange={e => setInclInTitle(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              id="exclFromTitle"
              name="exclFromTitle"
              multiline
              value={exclFromTitle}
              type="search"
              helperText="From Title"
              fullWidth
              variant="outlined"
              onChange={e => setExclFromTitle(e.target.value)}
            />
          </Grid>
          {/* spacer */}
          <Grid item xs={12} />
        </Grid>
      </form>
    </React.Fragment>
  );
}
