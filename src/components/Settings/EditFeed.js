/* eslint-disable no-script-url */

import React from "react";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Title from "./Title";

// Generate Order Data
function createData(id, date, name, shipTo, paymentMethod, amount) {
  return { id, date, name, shipTo, paymentMethod, amount };
}

const rows = [
  createData(
    0,
    "16 Mar, 2019",
    "Elvis Presley",
    "Tupelo, MS",
    "VISA ⠀•••• 3719",
    312.44
  ),
  createData(
    1,
    "16 Mar, 2019",
    "Paul McCartney",
    "London, UK",
    "VISA ⠀•••• 2574",
    866.99
  ),
  createData(
    2,
    "16 Mar, 2019",
    "Tom Scholz",
    "Boston, MA",
    "MC ⠀•••• 1253",
    100.81
  ),
  createData(
    3,
    "16 Mar, 2019",
    "Michael Jackson",
    "Gary, IN",
    "AMEX ⠀•••• 2000",
    654.39
  ),
  createData(
    4,
    "15 Mar, 2019",
    "Bruce Springsteen",
    "Long Branch, NJ",
    "VISA ⠀•••• 5919",
    212.79
  )
];

const useStyles = makeStyles(theme => ({
  seeMore: {
    marginTop: 5
  }
}));

function SimpleCard() {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          Info
        </Typography>
        <TextField
          id="standard-read-only-input"
          label="Link"
          helperText="https://www.kijiji.ca/rss-srp-chaussures-homme/grand-montreal/red-wing/k0c15117001l80002"
          className={classes.textField}
          margin="dense"
          InputProps={{
            readOnly: true
          }}
        />
      </CardContent>
    </Card>
  );
}

export default function Orders() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Edit Feed</Title>
      <form>
        <List>
          <ListItem>
            <TextField
              id="standard-name"
              label="Title"
              className={classes.textField}
              value="hi"
              margin="normal"
              helperText="feed link: https://www.kijiji.ca/rss-srp-chaussures-homme/grand-montreal/red-wing/k0c15117001l80002"
            />
          </ListItem>
          <ListItem>
            <SimpleCard />
          </ListItem>
          <Divider />
          <ListItem>
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
            >
              Add Filter
            </Button>
          </ListItem>
          <Divider />
          <ListItem>
            <Button
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Delete Feed
            </Button>
          </ListItem>
        </List>
      </form>
    </React.Fragment>
  );
}
