import React from "react";
// import { fireAuth } from "../../auth/fireApi";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import IconButton from "@material-ui/core/IconButton";
import SettingsIcon from "@material-ui/icons/Settings";
import Fab from "@material-ui/core/Fab";
import InputAdornment from "@material-ui/core/InputAdornment";
import Switch from "@material-ui/core/Switch";
import TextField from "@material-ui/core/TextField";
import Avatar from "@material-ui/core/Avatar";
import { Link } from "react-router-dom";
import "../../App.css";

const styles = theme => ({
  list: {
    width: 200
  },
  fullList: {
    width: "auto"
  },
  fab: {
    marginRight: 10,
    marginTop: 5,
    float: "right",
    zIndex: 10
  }
});

class SideDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      right: false,
      showModal: true
    };
  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };

  handleDistanceSubmit(e) {
    e.preventDefault();
    this.toggleDrawer("right", false);
  }

  onLogoutClick() {
    alert("loggin out...");
    fireAuth.signOut().then(this.props.history.push("/"));
  }

  render() {
    const { classes } = this.props;
    const sideList = (
      <div>
        <div className={classes.list}>
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" className={classes.title}>
                Photos
              </Typography>
              {true && (
                <div>
                  <IconButton
                    aria-owns={false ? "menu-appbar" : undefined}
                    aria-haspopup="true"
                    color="inherit"
                  />
                </div>
              )}
            </Toolbar>
          </AppBar>
          <form className={classes.container} noValidate autoComplete="off">
            <List>
              <ListItem key={0}>
                <Avatar
                  alt="user avatar"
                  src={this.props.user.photoURL}
                  className={classes.avatar}
                />
                <Button
                  variant="contained"
                  onClick={this.onLogoutClick}
                  size="small"
                >
                  Logout
                </Button>
              </ListItem>
              <ListItem key={1}>
                <TextField
                  id="distanceLimitInput"
                  label="Distance"
                  name="distanceLimit"
                  value={this.props.distanceLimit}
                  onChange={this.props.handleFormInputChange}
                  type="number"
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true
                  }}
                  margin="dense"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">km</InputAdornment>
                    )
                  }}
                  onSubmit={this.handleDistanceSubmit}
                />
                <Switch
                  color="primary"
                  checked={this.props.isDistanceEnabled}
                  name="isDistanceEnabled"
                  onChange={this.props.handleDistanceSwitch}
                  value={this.props.isDistanceEnabled}
                />
              </ListItem>
              <ListItem key={2}>
                <TextField
                  id="standard-select-currency"
                  select
                  label="Select"
                  className={classes.textField}
                  value={""}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu
                    }
                  }}
                  helperText="Please select your currency"
                  margin="normal"
                >
                  {[
                    { label: "Bikes", value: 0 },
                    { label: "RedFlagDeals", value: 1 },
                    { label: "Warehouse", value: 2 }
                  ].map(option => (
                    <ListItem key={option.value} value={option.value}>
                      {option.label}
                    </ListItem>
                  ))}
                </TextField>
              </ListItem>
            </List>
          </form>
          <Divider />
          <List>
            <ListItem
              button
              key={"settings"}
              onClick={this.showModal}
              component={Link}
              to="/settings"
            >
              <ListItemIcon>
                <SettingsIcon />
              </ListItemIcon>
              <ListItemText primary="Settings" />
            </ListItem>
          </List>
        </div>
      </div>
    );

    return (
      <div>
        <Fab
          size="small"
          color="inherit"
          aria-label="OpenMenu"
          className={classes.fab}
          onClick={this.toggleDrawer("right", true)}
        >
          <MenuIcon />
        </Fab>
        <SwipeableDrawer
          anchor="right"
          open={this.state.right}
          onClose={this.toggleDrawer("right", false)}
          onOpen={this.toggleDrawer("right", true)}
        >
          {sideList}
        </SwipeableDrawer>
      </div>
    );
  }
}

SideDrawer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SideDrawer);
